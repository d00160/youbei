package controllers

import (
	md "gitee.com/countpoison/youbei/models"
)

func (c *LoginController) DownloadFile() {
	id := c.Ctx.Input.Param(":id")
	log := new(md.Log)
	log.ID = id
	if err := log.Get(); err != nil {
		c.APIReturn(500, "查询失败", err)
	}
	c.Ctx.Output.Download(log.Localfilepath)
}
