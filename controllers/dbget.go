package controllers

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/xormplus/xorm"
)

type dbinfo struct {
	User     string
	Password string
	Char     string
}

func (c *MainController) DBGet() {
	User := c.GetString("User")
	Password := c.GetString("Password")
	Host := c.GetString("Host")
	Port := c.GetString("Port")
	Char := c.GetString("Char")

	db, err := xorm.NewEngine("mysql", User+":"+Password+"@("+Host+":"+Port+")/?charset="+Char)
	if err != nil {
		c.APIReturn(500, "连接数据库失败", err)
	}
	dbs := []string{}
	if err := db.SQL("show databases;").Find(&dbs); err != nil {
		c.APIReturn(500, "查询数据库失败", err)
	}

	c.APIReturn(200, "成功", dbs)
}
